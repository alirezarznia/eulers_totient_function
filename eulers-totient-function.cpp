// C program to calculate Euler's Totient Function
#include <stdio.h>
 
int phi(int n)
{
    int result = n; // Initialize result as n
 
    // Consider all prime factors of n and subtract their
    // multiples from result
    for (int p = 2; p * p <= n; ++p) {
         
        // Check if p is a prime factor.
        if (n % p == 0) {
             
            // If yes, then update n and result
            while (n % p == 0)
                n /= p;
            result -= result / p;
        }
    }
 
    // If n has a prime factor greater than sqrt(n)
    // (There can be at-most one such prime factor)
    if (n > 1)
        result -= result / n;
    return result;
}
 
// Driver program to test above function
int main()
{
    int n;
    for (n = 1; n <= 10; n++)
        printf("phi(%d) = %d\n", n, phi(n));
    return 0;
}


Some Interesting Properties of Euler’s Totient Function
1) For a prime number p, ?(p) is p-1. For example ?(5) is 4, ?(7) is 6 and ?(13) is 12. This is obvious, gcd of all numbers from 1 to p-1 will be 1 because p is a prime.

2) For two numbers a and b, if gcd(a, b) is 1, then ?(ab) = ?(a) * ?(b). For example ?(5) is 4 and ?(6) is 2, so ?(30) must be 8 as 5 and 6 are relatively prime.

3) For any two prime numbers p and q, ?(pq) = (p-1)*(q-1). This property is used in RSA algorithm.

4) If p is a prime number, then ?(pk) = pk – pk-1. This can be proved using Euler’s product formula.

5) Sum of values of totient functions of all divisors of n is equal to n.
gausss
For example, n = 6, the divisors of n are 1, 2, 3 and 6. According to Gauss, sum of ?(1) + ?(2) + ?(3) + ?(6) should be 6. We can verify the same by putting values, we get (1 + 1 + 2 + 2) = 6.

6) The most famous and important feature is expressed in Euler’s theorem :

The theorem states that if n and a are coprime
(or relatively prime) positive integers, then

a^(?(n)) ? 1 (mod n) 

The RSA cryptosystem is based on this theorem:

In the particular case when m is prime say p, Euler’s theorem turns into the so-called Fermat’s little theorem :

a^(p-1) ? 1 (mod p) 

7) Number of generators of a finite cyclic group under modulo n addition is ?(n).